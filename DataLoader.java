package com.osdb.starter.dataloader;

import com.osdb.starter.dataloader.consumers.UserDataLoader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

@Slf4j
@Component
@RequiredArgsConstructor
public class DataLoader {

    private static final String DATA_LOADER_LOCATION = "classpath:data";

    private final UserDataLoader userDataLoader;

    public void load() {
        final String PARENT_FOLDER = "common-data";
        loadData(getDataLoaderJsonFullPath(PARENT_FOLDER, "users"), userDataLoader);
    }

    private String getDataLoaderJsonFullPath(String parentFolder, String childFolder) {
        return String.format("%s/%s/%s/*.json", DATA_LOADER_LOCATION, parentFolder, childFolder);
    }

    private void loadData(String locationPattern, Consumer<Map<String, Object>> dataConsumer) {
        loadData(locationPattern, dataConsumer, null);
    }

    private void loadData(String locationPattern, Consumer<Map<String, Object>> dataLoaderConsumer, Consumer<List<Map<String, Object>>> dataCleanUpHandlerConsumer) {
        try {
            Resource[] resources = new PathMatchingResourcePatternResolver().getResources(locationPattern);
            List<Map<String, Object>> resourceObjectMaps = new ArrayList<>();

            for (Resource resource : resources) {
                JsonParser jsonParser = JsonParserFactory.getJsonParser();

                try (InputStream in = resource.getInputStream()) {
                    Map<String, Object> objectMap = jsonParser.parseMap(IOUtils.toString(in, StandardCharsets.UTF_8));
                    resourceObjectMaps.add(objectMap);
                    dataLoaderConsumer.accept(objectMap);
                }
            }

            if (dataCleanUpHandlerConsumer != null) {
                dataCleanUpHandlerConsumer.accept(resourceObjectMaps);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
