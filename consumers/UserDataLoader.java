package com.osdb.starter.dataloader.consumers;

import com.osdb.starter.common.util.Constants;
import com.osdb.starter.user.repository.UserRepository;
import com.osdb.starter.user.repository.entity.enumeration.Role;
import com.osdb.starter.user.repository.entity.user.User;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.Consumer;

import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class UserDataLoader implements Consumer<Map<String, Object>> {

    UserRepository userRepository;

    @Override
    public void accept(Map<String, Object> objectMap) {
        String email = (String) objectMap.get(Constants.User.USER_EMAIL);
        String role = (String) objectMap.get(Constants.User.USER_ROLE);

        if (userRepository.findByEmail(email).isPresent()) {
            return;
        }

        User user = User.builder()
                .email(email)
                .firstName(String.valueOf(objectMap.get(Constants.User.USER_FIRST_NAME)))
                .lastName(String.valueOf(objectMap.get(Constants.User.USER_LAST_NAME)))
                .role(Role.valueOf(role))
                .password(String.valueOf(objectMap.get(Constants.User.USER_PASSWORD)))
                .refCode(String.valueOf(objectMap.get(Constants.User.USER_REF_CODE)))
                .build();

        userRepository.save(user);
    }
}
