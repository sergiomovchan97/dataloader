package com.osdb.starter.dataloader;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

public interface DataLoaderInit extends ApplicationListener<ApplicationReadyEvent> {

    boolean isInitialized();
}
