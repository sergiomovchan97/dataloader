package com.osdb.starter.dataloader;

import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.WaitStrategies;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
@Component
@RequiredArgsConstructor
public class DataLoaderInitImpl implements DataLoaderInit{

    private final DataLoader dataLoader;

    private boolean isInitialized = false;

    private final Lock lock = new ReentrantLock();

    @Async
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        Retryer<Void> retryer = RetryerBuilder.<Void>newBuilder() //
                .retryIfException() //
                .withWaitStrategy(WaitStrategies.fixedWait(60, TimeUnit.SECONDS)) //
                .build();

        try {
            retryer.call(() -> {
                try {
                    log.info("dateloader.DataLoaderInit starting.");
                    dataLoader.load();

                    lock.lock();
                    try {
                        isInitialized = true;
                    } finally {
                        lock.unlock();
                    }
                    log.info("dateloader.DataLoaderInit finished.");
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    throw e;
                }
                return null;
            });
        } catch (ExecutionException | RetryException executionException) {
            log.error(executionException.getMessage(), executionException);
        }
    }

    @Override
    public boolean isInitialized() {
        lock.lock();
        try {
            return isInitialized;
        } finally {
            lock.unlock();
        }
    }
}
